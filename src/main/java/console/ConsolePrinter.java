package console;

import model.CarType;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static util.Constants.STOP_WRITING_TO_FILE_WORD;

public final class ConsolePrinter {

    private static final BufferedWriter WRITER;

    static {
        WRITER = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(FileDescriptor.out),
                        StandardCharsets.US_ASCII
                ), 512
        );
    }

    /**
     * A faster version of {@link System}'s println;
     * @param o - objects to be printed to standard output
     */
    public static void print(Object... o) {
        Arrays.stream(o).forEach(
                obj -> {
                    try {
                        WRITER.write(obj.toString() + "\n");
                        WRITER.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );

    }

    public static void printMessageBasedOnType(String type) {
        print("In order to indicate the end of your input, please use: " + STOP_WRITING_TO_FILE_WORD);
        switch (type) {
            case CarType.GAS -> print("""
                    For gas car, enter the following properties on the next line:
                    brand, model, engine displacement(liters), power(KW),  price 
                    """);
            case CarType.ELECTRIC -> print("""
                    For electric car, enter the following properties on the next line:
                    brand, model, power(KW), battery power(Ah), price
                    """);
            case CarType.HYBRID -> print("""
                    For hybrid car, enter the following properties on the next line:
                    brand, model, displacement(l), power(KW), battery power(Ah), price
                    """);
            default -> print("Invalid vehicle type!");

        }
    }

    public static void printOptions() {
        print("""
                Please make your choice:
                1 - Show the entire Mobility4U catalogue;
                2 - Add a new electric car;
                3 - Add a new gas car;
                4 - Add a new hybrid car;
                5 - Print the catalogue sorted by car-type;
                6 - Print the catalogue sorted by brand (alphabetically)
                7 - Write to file;
                8 - Stop the program.
                """);
    }

    public static void printLoadFileOptions() {
        print("""
                You need to specify a populated or empty csv catalog file
                whose contents are going to be loaded and mapped in the application.
                You can do this in two ways:
                 1. By modifying the application.properties file_path property and;
                 2. By specifying the absolute file path to the file, including file name and extension.
                 
                 If you choose option 1, before typing your choice, go and make the changes in application.properties.
                 Otherwise a default empty file in the default location is going to be loaded.
                 
                 Type 1 or 2 on the next line:  
                """);
    }
}
